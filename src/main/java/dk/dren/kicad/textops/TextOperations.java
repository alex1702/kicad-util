package dk.dren.kicad.textops;

import dk.dren.kicad.pcb.Board;
import dk.dren.kicad.pcb.FpText;
import lombok.Getter;
import org.decimal4j.immutable.Decimal6f;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

public class TextOperations implements Consumer<FpText> {
    @Getter
    List<TextOperation> operations = new ArrayList<>();

    public static TextOperations builder() {
        return new TextOperations();
    }

    public static void applyOperationsOnAllPossibleTexts(Board board, Map<String, TextOperations> ops) {

        board.modules().forEach(m->{
            for (Map.Entry<String, TextOperations> op : ops.entrySet()) {
                m.getFpText(op.getKey()).ifPresent(t -> op.getValue().accept(t));
            }
        });
    }

    @Override
    public void accept(FpText fpText) {
        operations.forEach(o->o.accept(fpText));
    }

    public TextOperations moveAwayFromOrigin(boolean move, double offset) {
        if (move) {
            operations.add(new MoveAwayFromOrigin(offset));
        }
        return this;
    }

    public TextOperations layer(String layerName) {
        if (layerName != null) {
            operations.add(new SetLayer(layerName));
        }
        return this;
    }

    public TextOperations hide() {
        operations.add(new Hide());
        return this;
    }

    public TextOperations show() {
        operations.add(new Show());
        return this;
    }

    public TextOperations hide(boolean hide) {
        if (hide) {
            hide();
        }
        return this;
    }

    public TextOperations show(boolean show) {
        if (show) {
            show();
        }
        return this;
    }

    public TextOperations size(Decimal6f size) {
        if (size != null) {
            operations.add(new SetSize(size));
        }
        return this;
    }

    public TextOperations moveBackToOrigin() {
        operations.add(new MoveAwayFromOrigin(0));
        return this;
    }
}

